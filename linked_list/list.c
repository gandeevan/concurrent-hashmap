/** @file   list.c
 *  @brief  This file implements the linked list library. 
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "list.h"

/**
 * @brief      Initialises a new linked list instance
 *
 * @return     pointer to the linked list instance
 */
LinkedList *ll_new(){

    /* glibc 2.2+ malloc is thread safe, for backwards compatibility compile and link with -pthreads */
    LinkedList *temp = (LinkedList *)malloc(sizeof(LinkedList));
    if(temp==NULL)
        return NULL;

    temp->head=NULL;

    return temp;
}

/**
 * @brief      destroys the linked list
 *
 * @param      list - pointer to the linked list instance
 * @return     void
 */
void ll_destroy(LinkedList *list){
    ListNode *curr=list->head;

    while(curr!=NULL){
        ListNode *temp = curr;
        curr=curr->next;
        free(temp);
    }

    return;
}


/**
 * @brief      inserts a new node into the linked list
 *
 * @param      list  - pointer to linked list instance
 * @param      key   - key to be inserted    
 * @param      value - value to be inserted
 *
 * @return     true on success and false on failure
 */
bool ll_insert(LinkedList *list, int key, int value){

    ListNode *temp, *head;

    temp = (ListNode *) malloc(sizeof(ListNode));

    if(temp==NULL)
        return false;

    temp->timestamp = time(NULL);
    temp->key = key;
    temp->value = value;
    temp->valid = true;
    temp->next = list->head;

    list->head = temp;
 
    return true;
}


/**
 * @brief      searches for a node associated with the given key in the linked 
 *             list and populates data with the value associated with the key
 *
 * @param      list - pointer to linked list instance
 * @param      key  - key to be searched 
 * @param      data - memory location where the value associated with the key is stored 
 *
 * @return     true if the key-value pair is found, else return false
 */
bool ll_search(LinkedList *list, int key, int *data){
    ListNode *curr = list->head;
    
    while(curr != NULL){
        if(curr->key == key && curr->valid == true){
            if(data!=NULL)
                (*data) = curr->value;
            return true;
        } else{
            curr = curr->next;
        }
    }

    return false;
}


/**
 * @brief      given a key, deletes the node associated with the key in the linked list
 *
 * @param      list - pointer to linked list instance
 * @param      key  - key to be deleted
 *
 * @return     true on success, false if a node associated with the key is not found
 */
bool ll_delete(LinkedList *list, int key){
    ListNode *curr = list->head;
    ListNode *prev = NULL;

    while(curr != NULL){
        if(curr->key == key && curr->valid==true){
            curr->valid = false;
            return true;
        } else{
            prev=curr;
            curr=curr->next;
        }
    }
    return false;
}

/**
 * @brief      removes the invalid nodes from the linked 
 *             list and frees up the associated memory.
 *
 * @param      list - pointer to linked list instance
 * 
 * @return     void
 */
void ll_free_invalid_nodes(LinkedList *list){
    ListNode *temp, *prev=NULL, *curr=list->head;

    while(curr!=NULL){
        if(curr->valid==false){
            temp=curr;

            if(prev==NULL){
                list->head = curr->next;
            } else{
                prev->next = curr->next;
            }
            curr = curr->next;
            free(temp);
        } else{
            prev=curr;
            curr = curr->next;
        }   
    }
}


/**
 * @brief      prints all the key,value pair in the linked list
 *
 * @param      list - pointer to linked list instance
 * 
 * @return     void
 */
void ll_print(LinkedList *list){
    ListNode *temp=list->head;

    while(temp!=NULL){
        printf("key=%d value=%d\n",temp->key,temp->value);
        temp=temp->next;
    }
}
