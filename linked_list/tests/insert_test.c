/**
 * @author 
 */

#include <stdio.h>
#include <assert.h>
#include "../list.h"

#define NUM_ELEMENTS 10

int main(){
    int key, value, i=0;
    LinkedList *list = ll_new();
    LinkedList_iterator it;

    for(i=0; i<NUM_ELEMENTS; i++){
        ll_insert(list,i*2,i);
    }

    if(!ll_init_iterator(list, &it, NULL))
        assert(false);

    i--;

    while(ll_has_next(&it)){
        ll_next(&it, &key, &value);
        

        if(key!=i*2 && value!=i){
            assert(("INSERT_TEST_FAILED" && false));
        }

        i--;
    }
    printf("INSERT_TEST_SUCCESS");    
}