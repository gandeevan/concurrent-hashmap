#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

#include "../list.h"

#define NUM_ELEMENTS 20
#define MAX_KEY ((NUM_ELEMENTS-1)*2)

int main(){
    int key, value, i;
    bool found;
    LinkedList *list = ll_new();

    srand(time(NULL));

    /* key is always even */  
    for(i=0; i<=MAX_KEY; i++){
        if(i%2==1)
            continue;
        else
            ll_insert(list,i,i*2);      
    }
    
    for(i=0; i<NUM_ELEMENTS; i++){

        key = rand()%(MAX_KEY*2);
        found = ll_search(list, key, &value);
    
        /* list shouldn't contain a node with a odd key and key is always <= MAX_KEY */
        if(found==true && (key%2==1 || key>MAX_KEY)){ 
            assert((false && "SEARCH_TEST_FAILED - FOUND INVALID KEY"));
        } else if(found==false && key%2==0 && key<=MAX_KEY){ /* should contain a node if key%2==0 and key<=MAX_KEY */
            assert((false && "SEARCH_TEST_FAILED - DIDN'T FIND VALID KEY"));
        } 
    }

    for(i=0;i<=MAX_KEY;i++){
        if(i%2==1)
            continue;
        else{
            if(!ll_search(list, i, NULL)){
                assert((false && "SEARCH_TEST_FAILED - DIDN'T FIND VALID KEY"));
            } 
        }
    }

    printf("SEARCH_TEST_SUCCESS");
}