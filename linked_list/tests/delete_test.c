#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

#include "../list.h"

#define NUM_ELEMENTS 20
#define MAX_KEY ((NUM_ELEMENTS-1)*2)

int main(){
    int key, value, i, key_count=0, it_count=0;
    bool found;
    LinkedList *list = ll_new();
    LinkedList_iterator it, it2;

     /* insert keys */  
    for(i=0; i<=(MAX_KEY/2); i++){
        key_count++;
        ll_insert(list,i,i*2);      
    }

    it_count = key_count;

    /* initialize the iterator */
    ll_init_iterator(list, &it2, NULL);
    
    srand(time(NULL));

    /* insert keys */  
    for(i=(MAX_KEY/2)+1; i<=MAX_KEY; i++){
        key_count++;
        ll_insert(list,i,i*2);      
    }

    /* iterate through keys - iterator should return only MAX_KEY/2 elements */
    while(ll_has_next(&it2)){
        it_count--;
        ll_next(&it2, &key, &value);
    }
   
    /* it_count should equals zero */
    if(it_count){
        printf("it_count = %d", it_count);
        assert((false && "DELETE_TEST_FAILED - CHECK_COUNT != KEY_COUNT")); 
    }

    /* reinitialize iterator */
    ll_init_iterator(list, &it, NULL);
    
    printf("\n\nITERATOR SHOULD RETURN KEYS FROM 0..%d\n", MAX_KEY);
    while(ll_has_next(&it)){
        it_count++;
        ll_next(&it, &key, &value);
        printf("KEY = %d, VALUE = %d\n", key, value);
    }

    /* it_count should equal key_count */
    if(it_count != key_count){
        printf("it_count = %d, key_count = %d", it_count, key_count);
        assert((false && "DELETE_TEST_FAILED - IT_COUNT != KEY_COUNT")); 
    }

    ll_init_iterator(list, &it, NULL);

    printf("\n\nDELETING ODD KEYS...\n");
   
    /* delete odd keys */
    for(int i=0; i<=MAX_KEY; i++){
        if(i%2==0)
            continue;

        if(ll_delete(list, i)==false){
            /* delete returned failure -  shouldn't happen here */
            assert((false && "DELETE_TEST_FAILED"));
        } else
            key_count--;
    }
    printf("ODD KEYS DELETED, OK");

    it_count=0;

    printf("\n\nITERATOR SHOULD RETURN ONLY EVEN KEYS FROM 0..%d\n", MAX_KEY);
    
    if(1){
        while(ll_has_next(&it)){
            it_count++;
            ll_next(&it, &key, &value);
            printf("KEY = %d, VALUE = %d\n", key, value);
            if(key%2==1){
                assert((false && "DELETE_TEST_FAILED - FOUND ODD KEY"));
            } else if(value != key*2){
                assert((false && "DELETE_TEST_FAILED - KEY VALUE INVARIANT NOT TRUE")); 
            }
        }
    } else{
        assert((false && "ITERATOR INITIALIZATION FAILED")); 
    }

    if(it_count != key_count)
        assert((false && "DELETE_TEST_FAILED - CHECK_COUNT != KEY_COUNT")); 


    printf("\n\nSEARCHING KEYS...\n");
    
    /* search for keys */
    for(i=0; i<=MAX_KEY; i++){

        key = i;
        found = ll_search(list, key, &value);
        
        /* should find even keys */
        if(key%2==0){
            if(found==false){
                printf("didn't find key %d\n", key);
                assert((false && "DELETE TEST FAILED"));
            }
        } else{  
            /* shouldn't find odd keys */

            if(found!=false){
                printf("found key %d\n", key);
                assert((false && "DELETE TEST FAILED"));
            }
           
        }

        fflush(stdout);
    }

    printf("FOUND ALL KEYS, OK!");

    printf("\n\nDELETING ODD KEYS...AGAIN\n");

    /* try deleting odd keys again, assert  */
    for(int i=0; i<=MAX_KEY; i++){
        if(i%2==0)
            continue;

        if(ll_delete(list, i)==true){
            assert(false);
        }
    }
    printf("DIDN'T FIND ODD KEYS...YAY !\n\n");

    printf("LINKED LIST BEFORE GARBAGE COLLECTION\n");
    ll_print(list);

    printf("\n\nLINKED LIST AFTER GARBAGE COLLECTION...SHOULD NOT CONTAIN ODD KEYS\n");
    ll_free_invalid_nodes(list);
    ll_print(list);

    printf("\n\nDELETE_TEST_SUCCESS");
}