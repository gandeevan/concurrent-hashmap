/** @file   list.h
 *  @brief  Function prototypes for the methods exported by the linked list library.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */

#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <time.h>

#define SUCCESS 0
#define FAILURE -1

typedef struct ListNode{
    int key;            /* key */ 
    int value;          /* value */
    bool valid;         /* validity of the node - set to false if the node is deleted */
    uint32_t timestamp; /* time when the node is inserted/deleted */
    struct ListNode *next; 
}ListNode;

typedef struct LinkedList{
    ListNode *head;
}LinkedList;

typedef struct LinkedList_iterator{
    uint32_t timestamp; /* iterator initialization timestamp */
    ListNode *curr;     
}LinkedList_iterator;



LinkedList* ll_new();


bool ll_insert(LinkedList *list, int key, int value);


bool ll_delete(LinkedList *list, int key);


bool ll_search(LinkedList *list, int key, int *data);


void ll_print(LinkedList *list);


bool ll_init_iterator(LinkedList *list, LinkedList_iterator *iterator, unsigned int *timestamp);


bool ll_has_next(LinkedList_iterator *iterator);


void ll_next(LinkedList_iterator *iterator, int *key, int *value);


void ll_free_invalid_nodes(LinkedList *list);


void ll_destroy(LinkedList *list);

#endif
