/** @file   list_iterator.c
 *  @brief  This file implements the iterator methods for the linked list library.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#include <stdbool.h>
#include "list.h"

/**
 * @brief      Initializes the linked list iterator.
 *
 * @param      list - pointer to the linked list instance
 * @param      iterator - pointer to the iterator instance
 * @param      timestamp - if NULL then the current timestamp is used, else use the provided timestamp
 *
 * @return     return true on success and false on failure
 */
bool ll_init_iterator(LinkedList *list, LinkedList_iterator *iterator, unsigned int *timestamp){
    if(list!=NULL){
        iterator->curr = list->head;

        if(timestamp != NULL)
            iterator->timestamp = *timestamp;
        else
            iterator->timestamp = time(NULL);
        
        return true;
    } else
        return false;
}

/**
 * @brief      returns true if the iteration has more elements.
 *
 * @param      list - pointer to the linked list instance
 *
 * @return     returns false if the iterator has traversed through 
 *             all the elements, else returns true
 */
bool ll_has_next(LinkedList_iterator *iterator){
    
    while(iterator->curr != NULL && 
            (iterator->curr->valid == false || 
                iterator->curr->timestamp > iterator->timestamp)) {
        
        iterator->curr = iterator->curr->next;
    
    }

    if(iterator->curr==NULL)
        return false;
    else
        return true;
}

/**
 * @brief      returns the next node in the linked list.
 *
 * @param      it  pointer to the iterator instance
 * @param      key - memory location to the key 
 * @param      value - memory location to store the value associated with the key
 */
void ll_next(LinkedList_iterator *iterator, int *key, int *value){
    if(key==NULL || value==NULL || iterator==NULL)
        return;

    (*key) = iterator->curr->key;
    (*value) = iterator->curr->value;

    iterator->curr = iterator->curr->next; 
    return;
}


