# README #

+### What is this repository for? ###

The HASHMAP library  implements a thread safe hash table that supports INSERT,
DELETE and FIND operations. The basic idea behind hash tables is to use a hash
function that maps a larger input space to smaller domain of bucket indices.

In  addition, the  hash table also supports an iterator which could be used to
traverse all the key value pairs in the hash table.

### Usage ###

*****INITIALIZE A MAP*****       
 
 m = map_new(NUM_BUCKETS);


*****INSERT KEY-VALUE PAIR*****

 map_insert(m, key, value);


*****SEARCH FOR KEY-VALUE PAIR*****
 
 int value, key = 5;
 map_search(m, key, &value);


*****DELETE KEY-VALUE PAIR *****

  map_delete(m, i);


*****INITIALIZE ITERATOR*****

  map_iterator it;
  map_iterator_init(m, &it);


*****ITERATE KEY_VALUE PAIRS*****
 
  while(map_has_next(&it))
 	 map_next(&it, &key, &value);
	
	
    
    
### Running the test suite ###

 1. make 
 2. python run_tests.py
 3. make clean
 
### Who do I talk to? ###

* Gandeevan Raghuraman
* gandeevan@cmu.edu