import subprocess

tests = ["insert_test", "insert_same_test", "iterator_test1", "iterator_test2",
        "delete_test", "delete_empty_test"]

for test in tests:
    print "\n\n******* running "+test+" *******\n"
    subprocess.call("./map/tests/"+test)
    print "\n"

