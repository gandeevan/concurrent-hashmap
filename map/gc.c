/** @file gc.c
 * 
 *  @brief This file implements the garbage collector module.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#include "map.h"

extern map *m;

/**
 * @brief      Entry point for the garbage collection module.
 *             The garbage collector wakes up every 30 seconds,
 *             performs garbage collection and goes back to sleep.
 *
 * @param      arg - dummy argument, never used.
 */

void gc_start(void *arg){

    while(1){
        int i=0;

        for(i=0; i<m->bucket_count; i++){
            /* try lock on the bucket */
            if(pthread_rwlock_trywrlock(&m->gc_locks[i]) == 0){
                /* successfully acquired trylock */
                ll_free_invalid_nodes(&m->buckets[i]);
                pthread_rwlock_unlock(&m->gc_locks[i]);
            }
        }
        /* run the garbage collector periodically */
        sleep(30);
    }

    return;
}