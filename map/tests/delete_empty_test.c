#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "../map.h"

#define gettid() syscall(SYS_gettid)


#define NUM_THREADS DEFAULT_NUM_BUCKETS
#define NUM_ELEMENTS DEFAULT_NUM_BUCKETS*10

map *m;

void thread_delete_start(void *arg){
    uint64_t tid;
    pthread_threadid_np(NULL, &tid);

    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=0; i<NUM_ELEMENTS; i++){
        if(map_delete(m, i)==true)
            assert(false);
    }
}

int main(){

    pthread_t threads[NUM_THREADS];
    map_iterator it;

    int key, value;


    m = map_new(DEFAULT_NUM_BUCKETS);
    
    if(m==NULL){
        assert(false);
    }


    /************************/
    /*    DELETION          */
    /************************/

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_delete_start, (void *)i) !=0)
            assert((false && "DELETE: PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }

    
    printf("DELETE_EMPTY TEST SUCCESS");
   
}