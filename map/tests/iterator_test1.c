#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "../map.h"

#define gettid() syscall(SYS_gettid)


#define NUM_THREADS DEFAULT_NUM_BUCKETS
#define NUM_ELEMENTS DEFAULT_NUM_BUCKETS*10

map *m;

void thread_start_run1(void *arg){
    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=0; i<NUM_ELEMENTS/2; i++){
        map_insert(m, i, i);
    }
}



void thread_start_run2(void *arg){
    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=NUM_ELEMENTS/2; i<NUM_ELEMENTS; i++){
        map_insert(m, i, i);
    }
}


int main(){

    int key, value;
    pthread_t threads[NUM_THREADS];
    map_iterator it;


    m = map_new(DEFAULT_NUM_BUCKETS);
    
    if(m==NULL){
        assert(false);
    }

    /* create threads and insert keys from [0..NUM_ELEMENTS/2) */
    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_start_run1, (void *)i) !=0)
            assert((false && "PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }

    printf("\nINSERT ROUND 1 COMPLETE !");

    /* initialize iterator */
    if(!map_iterator_init(m, &it))
        assert((false && "ITERATOR INITIALIZATION FAILED"));

    sleep(5);

    /* create threads and insert keys from [NUM_ELEMENTS/2..NUM_ELEMENTS) */
    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_start_run2, (void *)i) !=0)
            assert((false && "PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }

    printf("\nINSERT ROUND 2 COMPLETE !");

    int count = 0;

    /* iterator should return only keys from 0 to NUM_ELEMENTS/2 */
    while(map_has_next(&it)){
        map_next(&it, &key, &value);

        count++;

        /* invalid key */
        if(key>=NUM_ELEMENTS/2 || key != value){
            assert((false && "KEY VALUE INVARIANT NOT MET"));
        }
    }

    /* iterator should have NUM_ELEMENTS/2 elements */
    if(count != NUM_ELEMENTS/2){
        printf("COUNT = %d, NUM_ELEMENTS = %d", count, NUM_ELEMENTS/2);
        assert((false && "NUM_ELEMENTS/2 != COUNT"));
    }

    printf("\nITERATOR 1 CONSISTENT");

    /* initialize iterator again */
    if(!map_iterator_init(m, &it))
        assert((false && "ITERATOR INITIALIZATION FAILED"));

    count =0;

     /* this time iterator should return all keys */
     while(map_has_next(&it)){
        map_next(&it, &key, &value);

        count++;

        /* invalid key */
        if(key>=NUM_ELEMENTS || key != value){
            assert((false && "KEY VALUE INVARIANT NOT MET"));
        }
    }

    /* iterators should NUM_ELEMENTS elements */
    if(count != NUM_ELEMENTS){
        printf("COUNT = %d, NUM_ELEMENTS = %d", count, NUM_ELEMENTS);
        assert((false && "NUM_ELEMENTS != COUNT"));
    }

    printf("\nITERATOR 2 CONSISTENT");
        
    printf("\nITERATOR_TEST_1 SUCCESS");
  
}