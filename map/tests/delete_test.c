#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "../map.h"

#define gettid() syscall(SYS_gettid)


#define NUM_THREADS DEFAULT_NUM_BUCKETS
#define NUM_ELEMENTS DEFAULT_NUM_BUCKETS*10

map *m;

int inserted[NUM_ELEMENTS];
int miss[NUM_ELEMENTS];

void thread_insert_start(void *arg){

    uint64_t tid;
    pthread_threadid_np(NULL, &tid);

    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=0; i<NUM_ELEMENTS; i++){
        if(map_insert(m, i, i)==true)
            __sync_fetch_and_add(&inserted[i],1);
        else
            __sync_fetch_and_add(&miss[i],1);
    }
}

void thread_delete_start(void *arg){
    uint64_t tid;
    pthread_threadid_np(NULL, &tid);

    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=0; i<NUM_ELEMENTS; i++){
        if(map_delete(m, i)==true)
            __sync_fetch_and_sub(&inserted[i],1);
        else
            __sync_fetch_and_sub(&miss[i],1);
    }
}

int main(){

    pthread_t threads[NUM_THREADS];
    map_iterator it;

    int key, value;

    for(int i=0; i<NUM_ELEMENTS; i++){
        inserted[i]=0;
        miss[i]=0;
    }


    m = map_new(DEFAULT_NUM_BUCKETS);
    
    if(m==NULL){
        assert(false);
    }

    /************************/
    /*    INSERTION         */
    /************************/

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_insert_start, (void *)i) !=0)
            assert((false && "INSERT: PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }

    /* check if all insertions happened successfully */
    for(int i=0; i<NUM_ELEMENTS; i++){
        if(inserted[i]!=1){
            printf("%d NOT INSERTED IN THE MAP\n", i);
            assert(false);
        } 

        /* only 1 thread should have inserted into map, hence NUM_THREAD-1 threads should have missed */
        if(miss[i] != NUM_THREADS-1){
            printf("MISS[%d] = %d, thread_count = %d\n", i, miss[i], NUM_THREADS);
            assert(false);
        }
    }

    printf("INSERTIONS OK\n");

    /************************/
    /*    DELETION          */
    /************************/


    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_delete_start, (void *)i) !=0)
            assert((false && "DELETE: PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }

     /* check if all deletions happened successfully */
    for(int i=0; i<NUM_ELEMENTS; i++){
        if(inserted[i]!=0){
            printf("INSERTED[%d] != 0\n", i);
            assert(false);
        } 

        /* only 1 thread should have been able to delete from map, hence NUM_THREAD-1 threads should have missed */
        if(miss[i] != 0){
            printf("MISS[%d] = %d, thread_count = %d\n", i, miss[i], NUM_THREADS);
            assert(false);
        }
    }

    printf("DELETIONS OK\n");

    printf("DELETE TEST SUCCESS");
   
}