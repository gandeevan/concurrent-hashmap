#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include "../map.h"

#define gettid() syscall(SYS_gettid)


#define NUM_THREADS DEFAULT_NUM_BUCKETS
#define NUM_ELEMENTS DEFAULT_NUM_BUCKETS*10

map *m;

bool inserted[NUM_ELEMENTS];

void thread_start(void *arg){

    uint64_t tid;
    pthread_threadid_np(NULL, &tid);

    if(m==NULL)
        assert((false && "M==NULL"));

    for(int i=(int)arg; i<NUM_ELEMENTS; i+=NUM_THREADS){
        if(map_insert(m, i, i)==false){
            printf("THREAD %llu FAILED INSERTING %d\n", tid, i);
            assert(false);
        }
        inserted[i] = true;
    }
}

int main(){

    pthread_t threads[NUM_THREADS];
    map_iterator it;

    int key, value;

    for(int i=0; i<NUM_ELEMENTS; i++)
        inserted[i]=false;


    m = map_new(DEFAULT_NUM_BUCKETS);
    
    if(m==NULL){
        assert(false);
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        if(pthread_create(&threads[i], NULL, (void *)thread_start, (void *)i) !=0)
            assert((false && "PTHREAD_CREATE_FAILED"));
    }

    for(unsigned long int i=0; i<NUM_THREADS; i++){
        pthread_join(threads[i], NULL);
    }


    /* check if all insertions happened successfully */
    for(int i=0; i<NUM_ELEMENTS; i++){
        if(inserted[i]==false)
            printf("%d NOT INSERTED IN THE MAP\n", i);
    }

    printf("INSERTIONS OK");


    for(int key=0; key<NUM_ELEMENTS; key++){
        if(map_search(m, key, &value)==false){
            assert((false && "KEY NOT FOUND"));
        } else{
            if(key != value){
                assert((false && "KEY VALUE INVARIANT NOT MET"));
            } 
        }
    }
    printf("\nMAP CONSISTENCY OK");

    if(!map_iterator_init(m, &it))
        assert((false && "ITERATOR INITIALIZATION FAILED"));

    while(map_has_next(&it)){
        map_next(&it, &key, &value);

        if(key != value){
            assert((false && "KEY VALUE INVARIANT NOT MET"));
        }

        inserted[key] = false;
    }

    /* check if all keys were traversed */
    for(int i=0; i<NUM_ELEMENTS; i++){
        if(inserted[i]==true)
            printf("%d NOT TRAVERSED\n", i);
    }

    printf("\nALL KEYS TRAVERSED");
}