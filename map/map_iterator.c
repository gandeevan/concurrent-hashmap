/** @file   map_iterator.c
 *  @brief  This file contains the implementation for the map_iterator
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */

#include "map.h"

/**
 * @brief      Intializes the map iterator
 *
 * @param      m   pointer to the map instance
 * @param      it  pointer to the iterator instance
 *
 * @return     return true on success and false on failure.
 */
bool map_iterator_init(map *m, map_iterator *it){
   
    if(m==NULL)
        return false;

    it->current_bucket=0;
    it->m = m;
    it->timestamp = time(NULL);

    /* takes the gc read lock of the first bucket */
    pthread_rwlock_rdlock(&m->gc_locks[it->current_bucket]);

    return ll_init_iterator(&(it->m->buckets[it->current_bucket]), &it->ll_iterator, &it->timestamp);
}

/**
 * @brief      returns true if the iteration has more elements.
 * 
 *             The behavior is undefined if the iterator it hasn't been initialized 
 *             via a call to map_iterator_init.
 *
 * @param      it  pointer to the iterator instance
 *
 * @return     returns false if the iterator has traversed through 
 *             all the elements, else returns true
 */
bool map_has_next(map_iterator *it){
    if(it->m == NULL || it->current_bucket == it->m->bucket_count)
        return false;

    /* checks if the current bucket has more elements */
    if(ll_has_next(&it->ll_iterator)){
        return true;
    } else { /* current bucket has been completely traversed, switch to next bucket */
        do{
            /* release the garbage collector lock for the current bucket */
            pthread_rwlock_unlock(&it->m->gc_locks[it->current_bucket]);

            /* go to next bucket */
            it->current_bucket++;

            /* break the loop after iterator has looked through all buckets */
            if(it->current_bucket==it->m->bucket_count)
                break;
            
            /* acquire the lock for the next bucket */
            pthread_rwlock_rdlock(&it->m->gc_locks[it->current_bucket]);

            /* initialize the iterator for the bucket */
            ll_init_iterator(&(it->m->buckets[it->current_bucket]), &it->ll_iterator, &it->timestamp);

            if(ll_has_next(&it->ll_iterator))
                return true;
        }while(1);

        return false;
    }
}

/**
 * @brief      returns the next key-value pair in the hash table.
 * 
 *             The behavior is undefined if the iterator it hasn't been initialized 
 *             via a call to map_iterator_init.
 * 
 *             The behaviour of this function is undefined if the map_next is called without 
 *             invoking map_has_next or if most recent call to map_has_next returned false.  
 *
 * @param      it  pointer to the iterator instance
 * @param      key - memory location to the key 
 * @param      value - memory location to store the value associated with the key
 * 
 * @return     void
 */
void map_next(map_iterator *it, int *key, int *value){
    if(it==NULL || it->m==NULL || key==NULL || value==NULL)
        return;
    else
        return ll_next(&it->ll_iterator, key, value);
}

/**
 * @brief      destroys the iterator associated pointed to by it
 *
 *             The behavior is undefined on invoking map_iterator_destroy on a 
 *             already destroyed map_iterator or before calling 
 *             map_iterator_init.
 *             
 *             If a gc read lock is being held on a bucket, it's released 
 *             while destroying the iterator.
 *             
 *             Note: The application using the thread library should explicitly 
 *             call map_iterator_destroy to avoid memory leak.
 *             
 * @param      it    The iterator
 */
void map_iterator_destroy(map_iterator *it){
    
    /* check if gc lock on a bucket is being held */
    if(it->current_bucket != it->m->bucket_count)
        pthread_rwlock_unlock(&it->m->gc_locks[it->current_bucket]);

    it->current_bucket = INVALID;
}