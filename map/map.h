/** @file   map.h
 *  @brief  Function prototypes for the methods exported by the hashmap library.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#ifndef MAP_H
#define MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "../linked_list/list.h"


#define MAX_NUM_BUCKETS 4096       /* Maximum buckets */
#define DEFAULT_NUM_BUCKETS 128    /* Default number of buckets */          
#define INVALID -1


/* map definition */
typedef struct map{
    LinkedList *buckets;          /* bucket pointers */
    pthread_rwlock_t *rw_locks;   /* bucket locks to synchronize insert/delete/search */      
    pthread_rwlock_t *gc_locks;   /* garbage collection locks */
    unsigned int bucket_count;    /* number of buckets */
    pthread_t gc;                 /* thread id of the garbage collector */
}map;

/* iterator definition */
typedef struct map_iterator{
    map *m;                  /* pointer to the map */
    unsigned int timestamp;  /* time of iterator initialization */
    int current_bucket;      /* current bucket being traversed */
    LinkedList_iterator ll_iterator;  /* iterator to the current bucket */    
}map_iterator;


map* map_new(int bucket_count);
void map_destroy(map *map);
bool map_insert(map *map, int key, int value);
bool map_delete(map *map, int key);
bool map_search(map *map, int key, int *data);

bool map_has_next(map_iterator *it);
bool map_iterator_init(map *m, map_iterator *it);
void map_next(map_iterator *it, int *key, int *value);
void map_iterator_destroy(map_iterator *it);

#endif