/** @file map_internal.h
 *  @brief Function prototypes used by the hashmap library.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#ifndef MAP_INTERNAL_H
#define MAP_INTERNAL_H

#include "map.h"

void gc_start(void *arg);

#endif

