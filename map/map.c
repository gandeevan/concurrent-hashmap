/** @file  map.c
 *  @brief This file contains the hashmap imlementation.
 *
 *  @author Gandeevan Raghuraman
 *  @bug No known bugs.
 */


#include "map.h"
#include "map_internal.h"
#include <assert.h>



/**
 * @brief      Allocates and intializes a new map instance.
 *
 * @param[in]  bucket_count  number of buckets
 *
 * @return     returns NULL on failure, else returns 
 *             the address associated with the map instance.
 * 
 */
map* map_new(int bucket_count){

    int i;

    /* use default bucket count */
    if(bucket_count==INVALID)
        bucket_count = DEFAULT_NUM_BUCKETS;
    
    else if(bucket_count<=0 || bucket_count>MAX_NUM_BUCKETS)
        return NULL;
    
    /* allocate memory for the map data structure */
    map *m = (map *)malloc(sizeof(map));
    
    if(m==NULL)
        goto RETURN_FAILURE;

    m->gc_locks = NULL;
    m->rw_locks = NULL;
    m->bucket_count = bucket_count;

    /* allocate and initialize buckets */
    m->buckets = (LinkedList *)malloc(m->bucket_count * sizeof(LinkedList));

    if(m->buckets==NULL)
        goto RETURN_FAILURE;
    
    /* allocate memory for rwlocks of each bucket */
    m->rw_locks = (pthread_rwlock_t *)malloc(m->bucket_count * sizeof(pthread_rwlock_t));

    if(m->rw_locks==NULL)
        goto RETURN_FAILURE;

    /* allocate memory for gc locks of each bucket */
    m->gc_locks = (pthread_rwlock_t *)malloc(m->bucket_count * sizeof(pthread_rwlock_t));

    if(m->gc_locks==NULL)
        goto RETURN_FAILURE;
    

    for(i=0;i<bucket_count;i++){
        
        if(pthread_rwlock_init(&m->rw_locks[i], NULL)!=0)
            goto RETURN_FAILURE;

        if(pthread_rwlock_init(&m->gc_locks[i], NULL)!=0)
            goto RETURN_FAILURE;

        m->buckets[i].head = NULL;
    }

    if(pthread_create(&m->gc, NULL, (void *)gc_start, (void *) NULL) != 0){
        goto RETURN_FAILURE;
    }

    return m;

    RETURN_FAILURE:

    /* free allocated memory before returning failure */
    if(m != NULL){
        if(m->buckets != NULL)
            free(m->buckets);

        if(m->rw_locks != NULL)
            free(m->rw_locks);

        if(m->gc_locks != NULL)
            free(m->gc_locks);

        free(m);
    }
    return NULL;
}

/**
 * @brief      This function destroys the map and frees up the associated memory
 *
 *             The behaviour is undefined if the map is destroyed while other 
 *             threads are inserting/deleting/traversing/searching through 
 *             the hashmap.
 *             
 *             Acquires all the garbage collector locks to make sure, 
 *             the gc is not holding any locks and hence can be killed safely.
 *              
 * @param      map  - pointer to the map instance
 * 
 * @return     void
 */
void map_destroy(map *m){

    for(int i=0; i<m->bucket_count; i++){
        
        pthread_rwlock_wrlock(&m->gc_locks[i]);
        ll_destroy(&m->buckets[i]);
        
        /* not releasing locks so that garbage collector can terminate safely without cleanup */
    }


    if(pthread_cancel(m->gc) != 0){
        assert(false);
    }

    /* releasing the locks after gc has been killed, to ensure no one is holding the locks while the locks are destroyed */
    for(int i=0; i<m->bucket_count; i++){
        pthread_rwlock_unlock(&m->gc_locks[i]);
        pthread_rwlock_destroy(&m->gc_locks[i]);
        pthread_rwlock_destroy(&m->rw_locks[i]);
    }


    free(m->gc_locks);
    free(m->rw_locks);
    free(m->buckets);
    free(m);

}


/**
 * @brief      inserts a key-value pair in the map
 *
 * @param      map   - pointer to the map instance
 * @param      key   - key to be inserted
 * @param      value - value associated with the key
 *
 * @return     returns false if the key is already 
 *             present in the map, else returns true.
 */
bool map_insert(map *m, int key, int value){

    bool ret_val=false;

    if(m==NULL){
        return false;
    }

    int bucket_idx = key%m->bucket_count;
    LinkedList *list = &(m->buckets[bucket_idx]);

    pthread_rwlock_rdlock(&m->gc_locks[bucket_idx]);
    pthread_rwlock_wrlock(&m->rw_locks[bucket_idx]);

    if(!ll_search(&(m->buckets[bucket_idx]), key, NULL)){
        ret_val = ll_insert(list, key, value);
      
    }
 
    pthread_rwlock_unlock(&m->rw_locks[bucket_idx]);
    pthread_rwlock_unlock(&m->gc_locks[bucket_idx]);

    return ret_val;
}

/**
 * @brief      searches for the key and returns the value 
 *             associated with the key.
 *             
 * @param      map  - pointer to the map instance
 * @param      key  - key to be deleted
 * @param      data - memory location to store the value 
 *                    associated with the key
 *
 * @return     returns true if the key is found else returns false.
 */
bool map_search(map *m, int key, int *data){
    bool ret_val;

    if(m==NULL)
        return false;

    int bucket_idx = key%m->bucket_count;
    LinkedList *list = &(m->buckets[bucket_idx]);

    pthread_rwlock_rdlock(&m->gc_locks[bucket_idx]);
    pthread_rwlock_rdlock(&m->rw_locks[bucket_idx]);

    /* use rdlock for search */
    ret_val = ll_search(&(m->buckets[bucket_idx]), key, data);

    pthread_rwlock_unlock(&m->rw_locks[bucket_idx]);
    pthread_rwlock_unlock(&m->gc_locks[bucket_idx]);

    return ret_val;
}

/**
 * @brief      deletes the key-value pair associated with the provided key
 * 
 *             if the key is found, then the memory used to store the key-value 
 *             pair is not freed at the time of delete, it's garbage collected at
 *             a later point in time.
 *             
 *             delete just sets the invalid flag to true. 
 *    
 * @param      map - pointer to the map instance
 * @param      key - to be deleted
 *
 * @return     returns false if a node associated with the given key was not found, else returns true.
 */
bool map_delete(map *m, int key){
    bool ret_val;

    if(m==NULL)
        return false;

    int bucket_idx = key%m->bucket_count;
    LinkedList *list = &(m->buckets[bucket_idx]);

    pthread_rwlock_rdlock(&m->gc_locks[bucket_idx]);
    pthread_rwlock_wrlock(&m->rw_locks[bucket_idx]);


    ret_val = ll_delete(&(m->buckets[bucket_idx]), key);

    pthread_rwlock_unlock(&m->rw_locks[bucket_idx]);
    pthread_rwlock_unlock(&m->gc_locks[bucket_idx]);

    return ret_val;
}









